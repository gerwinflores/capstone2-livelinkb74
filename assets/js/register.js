let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {
	//prevents the form to revert it's default/ blank values
	e.preventDefault(); 

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let userEmail = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)){

		// fetch('url', {option})
		// to process a request
		fetch('https://cryptic-sierra-20304.herokuapp.com/api/users/email-exists', { //sending user information, nag fetch tayo ng request ito yung info na gusto natin ipasa mula sa routes back end
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'// ang data mo ay json format,same na nasa POSTMAN
			},
			body: JSON.stringify({
				email: userEmail
				// email="arvingmail" - magiging string type
			})
		})
		.then(res => res.json()) //binanbalik sa json format 
		.then(data => {
			console.log(data); //(true or false)

			if (data === false){

				fetch('https://cryptic-sierra-20304.herokuapp.com/api/users', { // route to register
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName : firstName,//galing sa backend .body
						lastName : lastName,
						email: userEmail,
						password : password1,
						mobileNo : mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data) //(true or false)

					if(data === true){
						alert("registered successfully");
						window.location.replace("login.html"); //window reffering to browser.pinapalitan yung location na key then nirereplace
					} else {
						alert("something went wrong");
					}
				})

			} else {
				alert("Email has been used. Try a different one")
			}

		})

	} else {
		alert("something went wrong")
	}
})

