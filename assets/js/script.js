let navItems = document.querySelector("#navSession");

//localStorage = an object used to store information in our client/devices
let userToken = localStorage.getItem("token");
console.log(userToken); 

// localStorage {
// 	getItem : function () => {
// 		print key
// 	}
// }

if(!userToken) {
	navItems.innerHTML =
		`	
			<li class="nav-item">
				<a href="./login.html" class="nav-link">
					login
				</a>
			</li>
		`

} else {
	navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link">
					logout
				</a>
			</li>

		`
}